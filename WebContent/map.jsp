<!DOCTYPE html>
<html>
  <head>
    <title>Place details</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        width: 75%;
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>
    <script src="js/jquery-1.11.1.min.js"></script>
    <script>
   
   <%-- $(document).ready(function(){
	    	
    	  var p=<%=request.getParameter("lat")%>;
    		var q=<%=request.getParameter("lan")%>;
    	
    		/*  var a = Integer.parseInt(lat,p);
    		 var b = Integer.parseInt(lan,q); */
    		  	  
		  initMap();
	  		
	  		
	      });  --%>
      function initMap() {
    	 alert(<%=request.getParameter("lat")%>);
        var map = new google.maps.Map(document.getElementById('map'), {
        	 center:  {lat: -25.363, lng: 131.044},
          zoom: 15
        });

        var infowindow = new google.maps.InfoWindow();
        var service = new google.maps.places.PlacesService(map);

        service.getDetails({
          placeId: 'ChIJN1t_tDeuEmsRUsoyG83frY4'
        }, function(place, status) {
          if (status === google.maps.places.PlacesServiceStatus.OK) {
            var marker = new google.maps.Marker({
              map: map,
              position: place.geometry.location
            });
            google.maps.event.addListener(marker, 'click', function() {
              infowindow.setContent('<div><strong>' + place.name + '</strong><br>' +
                'Place ID: ' + place.place_id + '<br>' +
                place.formatted_address + '</div>');
              infowindow.open(map, this);
            });
          }
        });
      }
    </script>
  </head>
  <body>
    <div id="map"></div>
    
   <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?v=3&sensor=false&libraries=places&key=AIzaSyCdRRKzAVZkGNYBx1vLeUgxzdHFWisygV8"></script>
  </body>
</html>