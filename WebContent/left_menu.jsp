
<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		 <form role="search">
			<div class="form-group">
			
				<!-- <input type="text" class="form-control" placeholder="Search"> -->
			</div>
		</form> 
		<ul class="nav menu">
	
			<li><a href="dashboard.jsp"><svg class="glyph stroked dashboard-dial"><use xlink:href="#stroked-dashboard-dial"></use></svg>Dashboard</a></li>
			<li><a href="cellinfo.jsp"><svg class="glyph stroked eye"><use xlink:href="#stroked-eye"></use></svg>Cell Information</a></li>
			<li><a href="cellcontact.jsp"><svg class="glyph stroked mobile device"><use xlink:href="#stroked-mobile-device"></use></svg>Cell Contacts</a></li>
			<li><a href="cellinbox.jsp"><svg class="glyph stroked app window with content"><use xlink:href="#stroked-app-window-with-content"></use></svg>Cell Inbox</a></li>
		    <li><a href="cellhistory.jsp"><svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"></use></svg>View History</a></li>
		
			<!-- <li><a href="forms.html"><svg class="glyph stroked pencil"><use xlink:href="#stroked-pencil"></use></svg> Forms</a></li>
			<li><a href="panels.html"><svg class="glyph stroked app-window"><use xlink:href="#stroked-app-window"></use></svg> Alerts &amp; Panels</a></li>
			<li><a href="icons.html"><svg class="glyph stroked star"><use xlink:href="#stroked-star"></use></svg> Icons</a></li> -->
			
	<!-- 		<li class="parent ">
				<a href="#">
					<span data-toggle="collapse" href="#sub-item-1"><svg class="glyph stroked chevron-down"><use xlink:href="#stroked-chevron-down"></use></svg></span> Dropdown 
				</a>
				<ul class="children collapse" id="sub-item-1">
					<li>
						<a class="" href="#">
							<svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> Sub Item 1
						</a>
					</li>
					<li>
						<a class="" href="#">
							<svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> Sub Item 2
						</a>
					</li>
					<li>
						<a class="" href="#">
							<svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> Sub Item 3
						</a>
					</li>
				</ul>
			</li> -->
			<li role="presentation" class="divider"></li>
			<li><a href="LogoutController?option=adminlogout"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Logout</a></li>
		</ul>

	</div><!--/.sidebar-->