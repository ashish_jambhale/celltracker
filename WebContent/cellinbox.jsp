<!DOCTYPE html>
<%@page import="bonrix.celltracker.model.CellData"%>
<%@page import="bonrix.celltracker.model.CellContact"%>
<%@page import="bonrix.celltracker.model.CellInbox"%>

<%@page import="java.util.List"%>
<%@page import="bonrix.celltracker.dao.CellDataDAO"%>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Cell Tracker</title>
<link rel="shortcut icon" href="images/celltrackerlogo1.png">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/datepicker3.css" rel="stylesheet">
<link href="css/styles.css" rel="stylesheet">
<link href="css/datepicker3.css" rel="stylesheet">



<!--Icons-->
<script src="js/lumino.glyphs.js"></script>



</head>

<body>
	<%@ include file="/header.jsp"%>
	<%@ include file="/left_menu.jsp"%>
		
	
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="dashboard.jsp"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
				<li class="active">Inbox</li>
			</ol>
		</div><!--/.row-->
				
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">Cell Inbox</div>
					<div class="panel-body">
						<table data-toggle="table" class='table table-striped table-hover table-bordered' id="apw" data-url=" "  " class="cell-border"   data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="false" data-pagination="false" data-sort-name="name" data-sort-order="desc">
						    <%
						    CellDataDAO celldataDAO = new CellDataDAO();
							List<CellInbox> dataList = celldataDAO.getInboxList();
								if (dataList.size() > 0) {
		%>
						    <thead>
						    <tr>
						       <th data-field="contact_id" data-sortable="true">Inbox_id:-</th>
						       <th data-field="sms_id" data-sortable="true">SMS_id:-</th>
						       <th data-field="imei" data-sortable="true">IMEI:-</th>
						       <th data-field="address" data-sortable="true">Address:-</th>
						       <th data-field="smsbody" data-sortable="true">SMS Body:-</th>
						       <th data-field="dateandtime" data-sortable="true">DateAndTIme:-</th>
						     <!--   <th data-field="search" data-sortable="false">Search:-</th> -->
						    </tr>
						    </thead>
						    
			<tbody>
			<%
				for (int index = 0; index < dataList.size(); index++) {
			%>
			<tr>
				<td ><%=dataList.get(index).getInbox_id()%></td>
				<td ><%=dataList.get(index).getSms_id()%></td>
				<td ><%=dataList.get(index).getImei()%></td>
				<td ><%=dataList.get(index).getSmsbody()%></td>
				<td ><%=dataList.get(index).getAddress()%></td>
				<td ><%=dataList.get(index).getDateandtime()%></td>
                <%-- <td> <button class="btn btn-primary" onclick="Search('<%=dataList.get(index).getImei()%>')">Search </button>	
				</td> 	 --%>
			</tr>
			<%
				}
							}
			%>
		</tbody>
						    
						    
						</table>
					</div>
				</div>
			</div>
		</div><!--/.row-->	
		
		
			</div>	<!--/.main-->

	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<!-- <script src="js/chart.min.js"></script> -->
	<!-- <script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script> -->
	<script src="js/bootstrap-datepicker.js"></script>
<script src="js/bootstrap-table.js"></script> 
	<link href="dataTables/dataTables.jqueryui.min.css" rel="stylesheet">
	<link href="dataTables/dataTables.bootstrap.css" rel="stylesheet">
<!-- 
<link href="dataTables/dataTables.responsive.css" rel="stylesheet"> -->
<link href="dataTables/dataTables.tableTools.min.css" rel="stylesheet">

<script src="dataTables/jquery.dataTables.js"></script>
<!-- <script src="dataTables/dataTables.bootstrap.js"></script> -->
<!--<script src="dataTables/dataTables.responsive.js"></script> -->
<script src="dataTables/dataTables.tableTools.min.js"></script>

	
	
	<script>
	$(document).ready( function () {
		   $('#apw').dataTable({ 
			 "bFilter": true,
			 "bInfo": true,
			 "bPaginate": true,
			   "sPaginationType" : "full_numbers", 
			   "oLanguage" : { 
			   "sLengthMenu" : "<span class='lenghtMenu' > _MENU_</span><span class='lengthLabel'></span>" 
			   }, 
			   "sDom": 'T<"clear">lfrtip', 
			   "oTableTools" : { 
			   "sSwfPath" : "dataTables/copy_csv_xls_pdf.swf" 
			   } 
			   });    
	} );
	
	
	
 
	
	
	
	
		!function ($) {
			$(document).on("click","ul.nav li.parent > a > span.icon", function(){		  
				$(this).find('em:first').toggleClass("glyphicon-minus");	  
			}); 
			$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
		
		
		
	var abc;	
		function Search(abc) {
alert(abc);
			
			var post_data1 = 
							 '&abc=' + abc+
							 '&option=SearchPing';
alert(post_data1);
			 $.ajax({
				type : "POST",
				url : "PingMasterController?" + post_data1,
				dataType : 'json',

				success : function(response) {
					if (response == null) {
						alert(" Response Not Found");
					} else {

						//var table= $('#example').DataTable(response);
						var table = $('#example').DataTable();
						table.clear().draw();
						table.rows.add(response.data);
						table.draw();

					}
				}

			}); 

			//alert ("data is filled");

		}
	</script>	
</body>

</html>
