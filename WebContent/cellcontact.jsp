<!DOCTYPE html>
<%@page import="bonrix.celltracker.model.CellData"%>
<%@page import="bonrix.celltracker.model.CellContact"%>
<%@page import="java.util.List"%>
<%@page import="bonrix.celltracker.dao.CellDataDAO"%>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Cell Tracker</title>
<link rel="shortcut icon" href="images/celltrackerlogo1.png">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/datepicker3.css" rel="stylesheet">
<link href="css/styles.css" rel="stylesheet">

<!--Icons-->
<script src="js/lumino.glyphs.js"></script>



</head>

<body>
	<%@ include file="/header.jsp"%>
	<%@ include file="/left_menu.jsp"%>
		
		
		
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="dashboard.jsp"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
				<li class="active">Contacts</li>
			</ol>
		</div><!--/.row-->
		
		

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">Cell Tracker</div>
					<div class="panel-body">
						<table   data-toggle=table class='table table-striped table-hover table-bordered' id="apw" data-url=" " class="cell-border"   data-show-refresh="false" data-show-toggle="false" data-show-columns="false" data-search="false" data-pagination="false">
					<center>
					<input type="text" name="" id="searchdata" 
					style="border: 1px solid #6297BC; width: 100px; border-radius: 4px;">
				
					<button type="button" onclick="reloadtable()" >Search</button>	  
				
					</center>
							    <thead>
						    <tr>
						   
						    <th data-field="contact_id" data-sortable="true" >Contact_id:-</th>
						    <th data-field="name" data-sortable="true">Name:-</th>
						    <th data-field="imei" data-sortable="true">IMEI:-</th>
						    <th data-field="mobileno" data-sortable="true">Mobile No:-</th>
						    <th data-field="what'sapp" data-sortable="true">What'sapp:-</th>
						    <th data-field="dateandtime" data-sortable="true">DateAndTIme:-</th>
						    <!--  <th data-field="search" data-sortable="false">DateAndTIme</th> -->
						    </tr>
						    </thead>
						    
			<tbody>
			
			<tr>
			
				<td ></td>
				<td ></td>
				<td ></td>
				
			
	
				<td ></td>
				<td ></td>
               <%--  <td> <button class="btn btn-primary" onclick="Search('<%=dataList.get(index).getImei()%>')">Search </button>	
				</td>  --%>	
			</tr>
			
		</tbody>
						    
						    
						</table>
					</div>
				</div>
			</div>
		</div><!--/.row-->	
		
		
		
		
		
		
		
			</div>	<!--/.main-->

	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<!-- <script src="js/chart.min.js"></script>
	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script> -->
	<script src="js/bootstrap-datepicker.js"></script>
	 <script src="js/bootstrap-table.js"></script>  
	
	<link href="dataTables/dataTables.jqueryui.min.css" rel="stylesheet">
	<link href="dataTables/dataTables.bootstrap.css" rel="stylesheet">
<!-- <link href="dataTables/dataTables.responsive.css" rel="stylesheet"> -->
<link href="dataTables/dataTables.tableTools.min.css" rel="stylesheet">

<script src="dataTables/jquery.dataTables.js"></script>
<!-- <script src="dataTables/dataTables.bootstrap.js"></script> -->
<!-- <script src="dataTables/dataTables.responsive.js"></script> --> 
<script src="dataTables/dataTables.tableTools.min.js"></script>
	

	<script>
	
	/* $(document).ready( function () {
		   $('#apw').dataTable({ 
			 
		   
		  
		   
	} ); */
	
	 
	   var oTable,nEditing,managerId,date;
    jQuery(document).ready(function() {
 	 
    
				getemplist();
             });
	
	
	var searchdata="NA";
	var table=null;
    function getemplist(){
   	
    
 	  // alert("getcontactcall");
 	   table= $('#apw').DataTable( {
 		  "bFilter": true,
			 "bInfo": true,
			 "processing": true,
		       "serverSide": true, 
			 "bPaginate": true,
			 "searching":false,
			
			 //  "sPaginationType" : "full_numbers", 
			   "oLanguage" : { 
			   "sLengthMenu" : "<span class='lenghtMenu' > _MENU_</span><span class='lengthLabel'></span>" 
			   }, 
			   "sDom": 'T<"clear">lfrtip', 
			   "oTableTools" : { 
			   "sSwfPath" : "dataTables/copy_csv_xls_pdf.swf" 
			   } ,
			   
			  
 		   "autoWidth": true,
	        "ajax": { "url": "CellContact.html",
	        "data": function ( d ) {
 	        	d.option="getContact";
 	        	d.searchdata=searchdata;
 	        	
	             
           }},
 	        	
	             
           
	        "fnCreatedRow":
	        	function( nRow, aData, iDataIndex ) {
	            $('td:eq(4)', nRow).html(getwhatsapp(aData[4]));
	           
	           

	        },
	    		
	        		
	        		
	          
	    } );
    }
	
	
    function getwhatsapp(datas){
    	
    //	alert(datas);
    
        //var Editing = 'Editing';
                 if(String(datas)=="NA"){
                       return " <img src='images/notwhatsapplogo.png' style='width: 40px;padding-left: 14px;'/>NA";
                   }
                 if(String(datas)=="WhatsApp"){
                     return "  <img src='images/whatsapplogo.png' style='width: 56px;'/>WhatsApp";
                 }
    }
		!function ($) {
			$(document).on("click","ul.nav li.parent > a > span.icon", function(){		  
				$(this).find('em:first').toggleClass("glyphicon-minus");	  
			}); 
			$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
		
	function reloadtable() {

		
			
	//		var searchdata=$("#searchdata").val() ;
	//	alert(searchdata);
				

	           		searchdata=$("#searchdata").val();
	           		
	           		table.ajax.reload();      
			   
			

		}
	
	
	</script>	
</body>

</html>
