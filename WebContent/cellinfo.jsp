<!DOCTYPE html>
<%@page import="bonrix.celltracker.model.CellData"%>
<%@page import="java.util.List"%>
<%@page import="bonrix.celltracker.dao.CellDataDAO"%>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Cell Tracker</title>
<link rel="shortcut icon" href="images/celltrackerlogo1.png">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/datepicker3.css" rel="stylesheet">
<link href="css/styles.css" rel="stylesheet">

<!--Icons-->
<script src="js/lumino.glyphs.js"></script>



</head>

<body>
	<%@ include file="/header.jsp"%>
	<%@ include file="/left_menu.jsp"%>
		
		
		
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="dashboard.jsp"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
				<li class="active">Infomation</li>
			</ol>
		<!-- <h5 style="color:green" id="data"></h5>  -->
		</div><!--/.row-->
		
		
		
		
		
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">Cell Tracker</div>
					<div class="panel-body">
						<table data-toggle="table" class='table table-striped table-hover table-bordered' id="apw" data-url=" " class="cell-border"   data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="false" data-pagination="false" data-sort-name="name" data-sort-order="desc">
						    <%
						    CellDataDAO celldataDAO = new CellDataDAO();
							List<CellData> dataList = celldataDAO.getDataList();
								if (dataList.size() > 0) {
		%>
						    <thead>
						    <tr>
						       <!--  <th data-field="state" data-sortable="true">tracker_id:-</th> -->
						       <th data-field="id" data-sortable="true">IMEI:-</th>
						        <th data-field="countryid" data-sortable="true">C_id:-</th>
						         <th data-field="mobileno" data-sortable="true">MobileNO:-</th>
						        <th data-field="operatername"  data-sortable="true">OPR:-</th>
						        <th data-field="imsi" data-sortable="true">IMSI:-</th> 
						       <th data-field="lac" data-sortable="true">Lac:-</th>
						        <th data-field="mcc" data-sortable="true">Mcc:-</th>
						         <th data-field="mnc" data-sortable="true">Mnc:-</th>
						        <th data-field="cell_id" data-sortable="true">Cellid:-</th>
						     
						           <th data-field="dateandtime" data-sortable="true">Date:-</th> 
						            <th data-field="accesstime" data-sortable="true">livetime:-</th>
						            <th data-field="aa" data-sortable="true">Details:-</th>
						            <!--  <th data-field="s" data-sortable="false">Inbox</th>
						              <th data-field="searcaa" data-sortable="false">Map</th> -->
						    </tr>
						    </thead>
						    
			<tbody>
			<%
				for (int index = 0; index < dataList.size(); index++) {
			%>
			<tr>
			<%-- 	<td ><%=dataList.get(index).getCelltracker_id()%></td> --%>
				<td ><%=dataList.get(index).getImei()%>
				
				 <button onclick="deleteinfo(<%=dataList.get(index).getImei()%>)" class="btn btn-danger">Delete</button>
				</td>
				<td ><%=dataList.get(index).getCountry_id()%></td>
				<td ><%=dataList.get(index).getMobileno()%></td>
				<td ><%=dataList.get(index).getOperatername()%></td>
				<td ><%=dataList.get(index).getImsi()%></td>
				<td ><%=dataList.get(index).getLac()%> </td>
				<td ><%=dataList.get(index).getMcc()%></td>
				<td ><%=dataList.get(index).getMnc()%></td>
			    <td><%=dataList.get(index).getCell_id()%>
	 <button href="#" data-toggle="modal" class="btn btn-success"  onclick="imeiset(<%=dataList.get(index).getImei()%>)" data-target="#addModal">
	 Gcm
		<!-- <svg class="glyph stroked location pin" style="width: 10px;" ><use xlink:href="#stroked-location-pin"/></svg> -->	
	</button> </td>
			    <td ><%=dataList.get(index).getDateandtime()%></td> 
                <td ><%=dataList.get(index).getAcccesstime()%></td>
			<%-- onclick="Search('<%=dataList.get(index).getImei()%>') --%>
			 <%-- <button class="btn btn-primary" href="imeicontact.jsp?imei=<%=dataList.get(index).getImei()%>">Search </button>	 --%>
	<td>	<a  title="contact"href="imeicontact.jsp?imei=<%=dataList.get(index).getImei()%>"><svg class="glyph stroked mobile device" style="width: 8px;color:#8B008B;"><use xlink:href="#stroked-mobile-device"/></svg>
</a>
	 <a title="inbox" href="imeiinbox.jsp?imei=<%=dataList.get(index).getImei()%>" > <svg   class="glyph stroked two messages" style="width: 15px;color:#004B49;"><use xlink:href="#stroked-two-messages"/></svg>
</a>	
		<%-- <a href="imeiinbox.jsp?imei=<%=dataList.get(index).getImei()%>">in
</a> --%>
	<a title="history" href="imeihistory.jsp?imei=<%=dataList.get(index).getImei()%>">
	<svg class="glyph stroked notepad " style="width: 12px;,color:#6F00FF;"><use xlink:href="#stroked-notepad"/></svg>
	
</a>
 <a title="map" target="_blank"href="http://maps.google.com/maps?q=<%=dataList.get(index).getLat()%>,<%=dataList.get(index).getLan()%>"> 
		<svg class="glyph stroked location pin" style="width: 10px;" ><use xlink:href="#stroked-location-pin"/></svg>	
	</a> 
	
	
	</td> 	
			</tr>
			<%
				}
							}
			%>
		</tbody>
						    
						    
						</table>
					</div>
				</div>
			</div>
		</div><!--/.row-->	
<script type="text/javascript">

function refresh(){
	$('#data').html("");
	 var post_data1 ='imei='+$('#imei').val()+
		'&option=refresh';

$.ajax({
type : "POST",
url : "CellData.html?"+ post_data1,
// dataType:"json",

success : function(response) {
	//alert(response);

	
	$('#data').html(response);

}	
}); 
		
	
}
		 function imeiset(imei)
{

			 $( "#imei" ).val(imei);
			 $('#data').html("");

}	
		 var msg;
		 function getgcm(msg){
			// $('#data').reset();
			
			 if(msg=="getping")
			 
			 {
				 var post_data1 ='imei='+$('#imei').val()+
				 '&message='+msg+
				 '&option=getgcm';

	 $.ajax({
		type : "POST",
		url : "CellData.html?"+ post_data1,
		// dataType:"json",

		success : function(response) {
			var json = $.parseJSON(response);

			
			$('#data').html(json.ping);
		
		}	
		}); 
				
				 
			 }
			 
			
			//alert($('#imei').val());
		// 	alert(msg);
		else{
		var post_data1 ='imei='+$('#imei').val()+
			 '&message='+msg+
			 '&option=getgcm';

 $.ajax({
	type : "POST",
	url : "CellData.html?"+ post_data1,


	success : function(response) {
		$('#data').html(response);
		//alert(response);
		//   window.setTimeout(function(){location.reload()},3000)
		
	}

});  
 
 
		 }
		 }
		
		 </script>
		
			</div>	<!--/.main-->
           <!-- Modal -->
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
 <div class="modal-dialog" role="document">
   <div class="modal-content animated flipInY">
     <div class="modal-header">
     GCM
       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
     </div>
     <div class="modal-body">
      <input type="hidden" id="imei">      
       <button type="button" class="btn btn-default" data-dismiss="modal" onclick="getgcm('getcontact')">Get Contact</button>
        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="getgcm('getsms')">Get SMS</button>
         <button type="button" class="btn btn-default" data-dismiss="modal" onclick="getgcm('getregistration')">Get Registration</button>
          <button type="button" class="btn btn-default" onclick="getgcm('getall')">Get All</button>
          <button type="button" class="btn btn-default"  onclick="getgcm('getping')">Get Ping</button>
  <h5 style="color:green" id="data"></h5><button class="btn btn-default" type="button" name="refresh" title="Refresh" onclick="refresh()"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
     </div>
     <div class="modal-footer">
       <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
     <!--   <button type="button" class="btn btn-primary">Save changes</button> -->
     </div>
   </div>
 </div>
</div>

	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<!-- <script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script> -->
	<!-- <script src="js/easypiechart-data.js"></script> -->
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/bootstrap-table.js"></script>
	
	
	
	
<link href="dataTables/dataTables.jqueryui.min.css" rel="stylesheet">
<link href="dataTables/dataTables.bootstrap.css" rel="stylesheet">
<!-- 
<link href="dataTables/dataTables.responsive.css" rel="stylesheet"> -->
<link href="dataTables/dataTables.tableTools.min.css" rel="stylesheet">

<script src="dataTables/jquery.dataTables.js"></script>
<!-- <script src="dataTables/dataTables.bootstrap.js"></script> --><!-- 
<script src="dataTables/dataTables.responsive.js"></script> -->
<script src="dataTables/dataTables.tableTools.min.js"></script>

	
	
	<script>
	$(document).ready( function () {
		   $('#apw').dataTable({ 
			 "bFilter": true,
			 "bInfo": true,
			 "bPaginate": true,
			 //  "sPaginationType" : "full_numbers", 
			   "oLanguage" : { 
			   "sLengthMenu" : "<span class='lenghtMenu' > _MENU_</span><span class='lengthLabel'></span>" 
			   }, 
			   "sDom": 'T<"clear">lfrtip', 
			   "oTableTools" : { 
			   "sSwfPath" : "dataTables/copy_csv_xls_pdf.swf" 
			   } 
			   });    
	} );
	
	
		!function ($) {
			$(document).on("click","ul.nav li.parent > a > span.icon", function(){		  
				$(this).find('em:first').toggleClass("glyphicon-minus");	  
			}); 
			$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
		
		
/* 	jQuery(document).ready(function() {
			
			 $("#myModal1").modal("show"); 	  
			$("#myModal1").modal("hide"); 
			 
			
		
			});	 */
	var imei;	
		function deleteinfo(imei) {
			
//alert(imei);

		 var post_data1 ='imei=' + imei+
						 '&option=deletedata';

			 $.ajax({
				type : "GET",
				url : "CellContact.html?"+ post_data1,
			//	dataType : 'json',

				success : function(response) {
			
					alert(response);
					   window.setTimeout(function(){location.reload()},1000)
					/* if (response == null) {
						alert(" Response Not Found");
					} else {

						//var table= $('#example').DataTable(response);
						var table = $('#example').DataTable();
						table.clear().draw();
						table.rows.add(response.data);
						table.draw();

					} */
				}

			}); 

			//alert ("data is filled");

		}
	</script>	
</body>

</html>
