package bonrix.celltracker.dao;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import bonrix.celltracker.model.CellContact;
import bonrix.celltracker.model.CellData;
import bonrix.celltracker.model.CellGcm;
import bonrix.celltracker.model.CellHistory;
import bonrix.celltracker.model.CellInbox;
import bonrix.celltracker.model.CellSecurity;
import bonrix.celltracker.model.DatatableJsonObject;


public class CellDataDAO {
	
	
	public int insertCellData(CellData celldata){
		
		//System.out.println(" in dao "+celldata.getImei());
		int success=0;
	    SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
	    Session session = sessionFactory.openSession();
	    Transaction txn = null;
	    try{
	        
	    	txn = session.beginTransaction();
	        session.save(celldata);
	        success=1;
	        txn.commit();
	        success=1;
	        
	    }catch(Exception e){
	        success=0;
	        e.printStackTrace();
	        session.getTransaction().rollback();
	    }finally {
	        if (!txn.wasCommitted()) {
	            txn.rollback();
	        }

	        session.flush();  
	        session.close();   
	    }
	    return success;
		
	   
	}
	 public int imeisearch(String imei){
		 
		  String json = null;
	     SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
	     Session session = sessionFactory.getCurrentSession();
	     DatatableJsonObject djson5=new DatatableJsonObject();
	     List result = null;
	    System.out.println("the data "+imei);
	         session.beginTransaction();
	        
	         Query query = session.createSQLQuery("SELECT * FROM celldata where imei="+imei+" ");
	         
	            result = query.list();
	         System.out.println("the size is "+result.size());   
	            return result.size();
	     } 
	      
	 public String updatecellData(CellData celldata){
		 

		        String json =null;
		        SessionFactory sf = HibernateUtil.getSessionFactory();
		        Session session = sf.openSession();
		        int result=0;
		        try{
		        	Transaction tx=  session.beginTransaction();

				     StringBuilder sb = new StringBuilder();
				     sb.append("update CellData set ");
				     
				     if(!celldata.getCountry_id().equalsIgnoreCase("NA")){
				    	  sb.append(" country_id ='"+celldata.getCountry_id()+"',");
				     }
				     
				     if(!celldata.getMobileno().equalsIgnoreCase("NA")){
				    	  sb.append(" mobileno ='"+celldata.getMobileno()+"',");
				     }
				     
				     if(!celldata.getOperatername().equalsIgnoreCase("NA")){
				    	  sb.append(" operatername ='"+celldata.getOperatername()+"',");
				     }
				     
				     if(!celldata.getImsi().equalsIgnoreCase("NA")){
				    	  sb.append(" imsi ='"+celldata.getImei()+"',");
				     }
				     if(!celldata.getLac().equalsIgnoreCase("NA")){
				    	  sb.append(" lac ='"+celldata.getLac()+"',");
				     }
				     if((!celldata.getMcc().equalsIgnoreCase("NA"))){
				    	 System.out.println("sdfdsg");
				    	  sb.append(" mcc ='"+celldata.getMcc()+"',");
				     }
				     if(!celldata.getMnc().equalsIgnoreCase("NA")){
				    	  sb.append(" mnc ='"+celldata.getMnc()+"',");
				     }
				     if(!celldata.getCell_id().equalsIgnoreCase("NA")){
				    	  sb.append(" cell_id ='"+celldata.getCell_id()+"',");
				     }
				     
				     if(!celldata.getLat().equalsIgnoreCase("NA")){
				    	  sb.append(" lat ='"+celldata.getLat()+"',");
				    	  
				    	  
				     }
				     if(!celldata.getLan().equalsIgnoreCase("NA")){
				    	  sb.append("lan ='"+celldata.getLan()+"',");
				    	  
				    	  
				     } if(!celldata.getGoogle_key().equalsIgnoreCase("NA")){
				    	  sb.append("google_key ='"+celldata.getGoogle_key()+"',");
				    	  
				    	  
				     }
				     
				     
				     sb.append("acccesstime='"+celldata.getAcccesstime()+"'");
				    // sb.append("dateandtime ='"+celldata.getDateandtime()+"'");
				     sb.append(" where imei ='"+celldata.getImei()+"'");
				
		           session.createQuery(sb.toString()).executeUpdate();
		            tx.commit();
		            
		        }catch(Exception e){
		            e.printStackTrace();
		            session.getTransaction().rollback();
		        }
		        session.close();
		        return "SucessFully Update";
		    }
		    
	 public int insertCellContact(CellContact cellcontact){
			
			//System.out.println(" in dao "+celldata.getImei());
			int success=0;
		    SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		    Session session = sessionFactory.openSession();
		    Transaction tx= session.beginTransaction();
		    try{
		        
		    	tx = session.beginTransaction();
		    	Query query = session.createSQLQuery("INSERT IGNORE INTO cellcontact(imei,mobileno,NAME,dateandtime,whatsapp) VALUES('"+cellcontact.getImei()+"','"+cellcontact.getMobileno()+"','"+cellcontact.getName()+"',NOW(),'"+cellcontact.getWhatsapp()+"')");
		    	query.executeUpdate();
		     //	session.save(cellcontact);
		        tx.commit();
		        success=1;
		        
		    }catch(Exception e){
		        success=0;
		        e.printStackTrace();
		        session.getTransaction().rollback();
		    }finally{
		    	session.close();
		    	 	
		    }
		    return success;
			
		   
		}
	 public int insertCellInbox(CellInbox cellinbox){
			
			//System.out.println(" in dao "+celldata.getImei());
			int success=0;
			
		    SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		    Session session = sessionFactory.openSession();
		    Transaction tx= session.beginTransaction();
		    try{
		        
		    	tx = session.beginTransaction();
		    	Query query = session.createSQLQuery("INSERT IGNORE INTO cellinbox(sms_id,imei,address,smsbody,dateandtime) VALUES('"+cellinbox.getSms_id()+"','"+cellinbox.getImei()+"','"+cellinbox.getAddress()+"','"+cellinbox.getSmsbody()+"',NOW())");
		    	query.executeUpdate();
		    	 
		    	//session.save(cellinbox);
		        tx.commit();
		        success=1;
		        
		    }catch(Exception e){
		        success=0;
		       System.out.println("failed");
		        e.printStackTrace();
		        session.getTransaction().rollback();
		    }finally{
		    	session.close();
		    		
		    }
		    return success;
			
		   
		}
		 
 public int imeimobilesearch(CellContact cellcontact){
		 
		 String json = null;
	     SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
	     Session session = sessionFactory.getCurrentSession();
	     DatatableJsonObject djson5=new DatatableJsonObject();
	     List result = null;
	    
	         session.beginTransaction();
	        
	         Query query = session.createSQLQuery("SELECT * FROM cellcontact where imei="+cellcontact.getImei()+" and mobileno="+cellcontact.getMobileno()+" ");
	         
	            result = query.list();
	     //    System.out.println("the size is "+result.size());   
	            return result.size();
	     } 
	 
	 
 public int smsidimeiserarch(CellInbox cellinbox){
	 
	 String json = null;
     SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
     Session session = sessionFactory.getCurrentSession();
     DatatableJsonObject djson5=new DatatableJsonObject();
     List result = null;
    
       session.beginTransaction();
        
         Query query = session.createSQLQuery("SELECT * FROM cellinbox where imei="+cellinbox.getImei()+" and sms_id="+cellinbox.getSms_id()+" ");
         
            result = query.list();
         System.out.println("the size is "+result.size());   
            return result.size();
     } 
 
 @SuppressWarnings("unchecked")
	public List<CellData> getDataList()  {
		   
		 SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
	     Session session = sessionFactory.getCurrentSession();
	         session.beginTransaction();
	        
	         List<CellData> lst=session.createCriteria(CellData.class).list();
	         session.getTransaction().commit();
	         
	     
		
	         return lst;
	         
	}		 
 /*@SuppressWarnings("unchecked")
	public List<CellContact> getContactList()  {
		   
		 SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
	     Session session = sessionFactory.getCurrentSession();
	   
	         session.beginTransaction();
	        
	         Criteria cr = session.createCriteria(CellContact.class);
	         cr.addOrder(Order.asc("dateandtime"));
	         cr.setMaxResults(1000);
	         List<CellContact> lst = cr.list();
	      
	         
	         //  List<CellContact> lst=session.createCriteria(CellContact.class).addOrder(Order.asc("datenandtime")).list();
	         
	         session.getTransaction().commit();
	         
	     
		
	         return lst;
	         
	}		 
*/ 
 
 public String getcontactlist(int page,int listSize,int start)
 {
	 String json = null;
     SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
     Session session = sessionFactory.getCurrentSession();
     DatatableJsonObject djson3=new DatatableJsonObject();
     List result = null;
 try{
    	 
         session.beginTransaction();
         
         Query	q=session.createSQLQuery("select COUNT(*) from CellContact");
 		BigInteger count = (BigInteger) q.uniqueResult();
 		
      
      //   Query query = session.createSQLQuery("");
         List lst = session.createSQLQuery("SELECT contact_id,name,imei,mobileno,whatsapp,dateandtime FROM CellContact ORDER BY dateandtime ASC").setMaxResults(listSize)
  				.setFirstResult((page - 1) * listSize).list();
  		
  	
     	DatatableJsonObject pjo = new DatatableJsonObject();
 		pjo.setRecordsFiltered(count.intValue());
 		pjo.setRecordsTotal(count.intValue());
 		pjo.setData(lst);
 		
 		Gson gson  = new GsonBuilder().setPrettyPrinting().create();
 		
          
           
          
 		 return gson.toJson(pjo).toString();	          
	         /*djson3.setRecordsTotal(result.size());
	         djson3.setRecordsFiltered(10);
	         djson3.setData(result);
	         session.getTransaction().commit();
	         
	         return new Gson().toJson(djson3);	     */    
          //  djson3.setRecordsTotal(result.size());
          //  djson3.setRecordsFiltered(result.size());
      
     }catch(Exception e){
         e.printStackTrace();
         session.getTransaction().rollback();
     }
return json;

   
  
	 
 }
 
 public String getsearchlist(int page,int listSize,int start,String searchdata)
 {
	 String json = null;
     SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
     Session session = sessionFactory.getCurrentSession();
     DatatableJsonObject djson3=new DatatableJsonObject();
     List result = null;
 try{
    	 
         session.beginTransaction();
         String pa="select COUNT(*)  FROM CellContact where  name LIKE '"+searchdata+"%' OR `imei` LIKE '"+searchdata+"%' OR `mobileno` LIKE '"+searchdata+"%'";
         System.out.println(""+pa);
         Query	q=session.createSQLQuery("select COUNT(*) from cellcontact  where  name LIKE '"+searchdata+"%' OR `imei` LIKE '"+searchdata+"%' OR `mobileno` LIKE '"+searchdata+"%'");
 		BigInteger count = (BigInteger) q.uniqueResult();
 		
      
      //   Query query = session.createSQLQuery("");
 		String qw="SELECT contact_id,name,imei,mobileno,whatsapp,dateandtime FROM cellcontact name LIKE '"+searchdata+"%' OR `imei` LIKE '"+searchdata+"%' OR `mobileno` LIKE '"+searchdata+"%' ";
         
 		System.out.println(""+qw);
 		List lst = session.createSQLQuery("SELECT contact_id,name,imei,mobileno,whatsapp,dateandtime FROM CellContact where  name LIKE '"+searchdata+"%' OR `imei` LIKE '"+searchdata+"%' OR `mobileno` LIKE '"+searchdata+"%' ").setMaxResults(listSize)
  				.setFirstResult((page - 1) * listSize).list();
  		
  	
     	DatatableJsonObject pjo = new DatatableJsonObject();
 		pjo.setRecordsFiltered(count.intValue());
 		pjo.setRecordsTotal(count.intValue());
 		pjo.setData(lst);
 		
 		Gson gson  = new GsonBuilder().setPrettyPrinting().create();
 		
          
           
          
 		 return gson.toJson(pjo).toString();	          
	         /*djson3.setRecordsTotal(result.size());
	         djson3.setRecordsFiltered(10);
	         djson3.setData(result);
	         session.getTransaction().commit();
	         
	         return new Gson().toJson(djson3);	     */    
          //  djson3.setRecordsTotal(result.size());
          //  djson3.setRecordsFiltered(result.size());
         
	         
         
        
         
     }catch(Exception e){
         e.printStackTrace();
         session.getTransaction().rollback();
     }
return json;

   
  
	 
 }
 
 
 @SuppressWarnings("unchecked")
	public List<CellInbox> getInboxList()  {
		   
		
		 SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
	     Session session = sessionFactory.getCurrentSession();
	   
	     
	         session.beginTransaction();
	        
	         Criteria cr = session.createCriteria(CellInbox.class);
	         cr.addOrder(Order.asc("dateandtime"));
	         cr.setMaxResults(1000);
	         List<CellInbox> lst = cr.list();
	        // List<CellInbox> lst=session.createCriteria(CellInbox.class).list();

	         session.getTransaction().commit();
	         
	     
	         return lst;
	         
	}		 
	 
 @SuppressWarnings("unchecked")
	public List<CellContact> getimeiContactList(String imei)  {
		   
		
		
		 SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
	     Session session = sessionFactory.getCurrentSession();
	   
	     
	         session.beginTransaction();
	         Criteria cr = session.createCriteria(CellContact.class);
	         cr.add(Restrictions.eq("imei", imei));
	         
	         List results = cr.list();
	        session.getTransaction().commit();
	         
	   
		
	         return results;
	         
	}		 
			
 @SuppressWarnings("unchecked")
	public List<CellInbox> getimeiInboxList(String imei)  {
		   
		 SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
	     Session session = sessionFactory.getCurrentSession();
	   
	         session.beginTransaction();
	         Criteria cr = session.createCriteria(CellInbox.class);
	         cr.add(Restrictions.eq("imei", imei));
	         List results = cr.list();
	         session.getTransaction().commit();
	         
	         return results;
	         
	}		 
			
 public int insertHistory(CellHistory cellhistory){
		
		int success=0;
	    SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
	    Session session = sessionFactory.openSession();
	    Transaction txn = null;
	    try{
	        
	    	txn = session.beginTransaction();
	    	
	    	
	        session.save(cellhistory);
	        success=1;
	        txn.commit();
	        success=1;
	        
	    }catch(Exception e){
	        success=0;
	        e.printStackTrace();
	        session.getTransaction().rollback();
	    }finally {
	        if (!txn.wasCommitted()) {
	            txn.rollback();
	        }

	        session.flush();  
	        session.close();   
	    }
	    return success;
		
	   
	}
 
 
 @SuppressWarnings("unchecked")
	public List<CellHistory> getHistoryList()  {
		   
		 SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
	     Session session = sessionFactory.getCurrentSession();
	   
	       session.beginTransaction();
	        
	            
	         List<CellHistory> lst=session.createCriteria(CellHistory.class).list();
	         session.getTransaction().commit();
	         
	         return lst;
	         
	}		 
 
 
 @SuppressWarnings("unchecked")
	public List<CellHistory> getimeiHistoryList(String imei)  {
		   
		 SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
	     Session session = sessionFactory.getCurrentSession();
	   
	     
	         session.beginTransaction();
	         Criteria cr = session.createCriteria(CellHistory.class);
	         cr.add(Restrictions.eq("imei", imei));
	         List results = cr.list();
	         session.getTransaction().commit();
	  	
	         return results;
	         
	}		 
			

public String deletedata(String imei){
	 

     SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
     Session session = sessionFactory.openSession();
   
    System.out.println("dsfdsfd"+imei);
       Transaction tx=  session.beginTransaction();
   
       String query1="delete  from CellInbox where imei=:imei";
       String query2="delete  from CellContact where imei=:imei";
       String query3 = "delete from CellData where imei = :imei";
       session.createQuery(query1).setString("imei", imei).executeUpdate();
       session.createQuery(query2).setString("imei", imei).executeUpdate();
       session.createQuery(query3).setString("imei", imei).executeUpdate();
       tx.commit();
       session.close();
        
            return "Sucessfully deleted";
     } 
 
public CellGcm getmasterkey() {
	 String json = null;
	 
	    SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
	    Session session = sessionFactory.openSession();
	    
	    List result = null;
	    CellGcm cm=null;
	    try{
	    	Transaction tx=      session.beginTransaction();
	        Query query = session.createQuery("from CellGcm");
	      
	        // System.out.println(query);
	        cm = (CellGcm) query.list().get(0);
	     
	        tx.commit(); 
	   	    }
	    catch(Exception e){
	        e.printStackTrace();
	        session.getTransaction().rollback();
	    }
	    session.close();
	    return cm;
	   }
public CellData getsuspectkey(String imei) {
	 String json = null;
	    SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
	    Session session = sessionFactory.openSession();
	    List result = null;
	    CellData cd=null;
	    try{
	    	Transaction tx= session.beginTransaction();
	        Query query = session.createQuery("from CellData where imei='"+imei+"'");
	       // System.out.println(query);
	      
	        cd = (CellData) query.list().get(0);
	       tx.commit();
	  
	    }catch(Exception e){
	        e.printStackTrace();
	        session.getTransaction().rollback();
	    }
	    session.close();
	    return cd;
	   }
 
public String updateacccesstime(String imei, String msg){
	 
    SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
    Session session = sessionFactory.openSession();
  
  System.out.println("dsfdsfd"+imei);
      Transaction tx=  session.beginTransaction();
  
    
      Date date = new Date();
	  SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//dd/MM/yyyy
	   
	  Date now = new Date();
	  String strDate = sdfDate.format(now);
	  Timestamp ts = Timestamp.valueOf(strDate);
   
	  Query query = session.createQuery(" update CellData set acccesstime =:acccesstime,pingstatus=:pingstatus where imei=:imei");
      query.setParameter("imei",imei);
      query.setTimestamp("acccesstime",ts);
      query.setParameter("pingstatus", msg);
    
      query.executeUpdate();
      tx.commit();
      session.close();
       
           return "Success Sucessfully updated";
    } 
 public String setpingstatus(String imei)
 {

	    SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
	    Session session = sessionFactory.openSession();
	 
	      Transaction tx=  session.beginTransaction();
	      Query query = session.createQuery("update CellData set pingstatus=:pingstatus where imei=:imei");
    
	 query.setParameter("imei",imei);
     query.setParameter("pingstatus","offline");
     query.executeUpdate();
     tx.commit();
    session.close();
	 return "SUCCESSS updated";
 
 }
 
 
 public int insertSecurity(CellSecurity cellsecurity){
		
	// System.out.println(""+cellsecurity.getKey());
			
			
			int success=0;
		    SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		    Session session = sessionFactory.openSession();
		    Transaction txn = null;
		    try{
		        
		    	txn = session.beginTransaction();
		    
		    	session.save(cellsecurity);
		        success=1;
		        txn.commit();
		        success=1;
		        
		    }catch(Exception e){
		        success=0;
		        e.printStackTrace();
		        session.getTransaction().rollback();
		    }finally {
		        if (!txn.wasCommitted()) {
		            txn.rollback();
		        }

		        session.flush();  
		        session.close();   
		    }
		    return success;
		   
	}
 
public int securityserarch(CellSecurity cellsecurity){
	 
	 String json = null;
     SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
     Session session = sessionFactory.openSession();
     DatatableJsonObject djson5=new DatatableJsonObject();
     List result = null;
    
       session.beginTransaction();
        
         Query query = session.createSQLQuery("SELECT * FROM cellsecurity where token='"+cellsecurity.getToken()+"' and keyvalue='"+cellsecurity.getKeyvalue()+"' ");
         
            result = query.list();
         System.out.println("the size is "+result.size());   
            return result.size();
     } 
public CellSecurity getCellSecuritykey(String token) {
    String json = null;
    SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
    Session session = sessionFactory.getCurrentSession();
    List result = null;
    CellSecurity key=null;
    try{
        session.beginTransaction();
        Query query = session.createQuery("from CellSecurity where token='"+token+"'");
       // System.out.println(query);
        key = (CellSecurity) query.list().get(0);
      //  json = new Gson().toJson(result);
        session.getTransaction().commit();
       // System.out.println("json"+json);
    }catch(Exception e){
        e.printStackTrace();
        session.getTransaction().rollback();
    }
    return key;
}

}
			 