package bonrix.celltracker.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import bonrix.celltracker.dao.CellDataDAO;
import bonrix.celltracker.model.CellData;
import bonrix.celltracker.model.CellHistory;


public class CellDataController extends HttpServlet {
	
	
	/*protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
	
		 System.out.println("dgvdf");
		response.sendRedirect("/login.jsp");
		
	}*/
	
	private static final long serialVersionUID = 1L;

	@Override
	public void init() throws ServletException {
		// Servlet initialization code here
		super.init();
	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
        
	//http://localhost:8080/celltracker1/CellData.html?imei=123.23.34.1&operatername=1566&imsi=45555&lac=3232&mcc=23243&mnc=34323&cell_id=1233343&lat=32434&lat=32434&dateandtime=2016-12-12 14:46:54
		 CellDataDAO celldataDAO = new CellDataDAO();
		 
		 PrintWriter out = response.getWriter();
		 
		 response.setContentType("text/html");
		
		String imei = request.getParameter("imei");
		String country_id = request.getParameter("country_id");
		String mobileno = request.getParameter("mobileno");
		System.out.println("the mobile"+mobileno);
		String operatername= request.getParameter("operatername");
	    String  imsi= request.getParameter("imsi");
	    String  lac= request.getParameter("lac");
	    String  mcc= request.getParameter("mcc");
	    String  mnc= request.getParameter("mnc");
	    String  cell_id= request.getParameter("cell_id");
	    String  lat= request.getParameter("lat");
	    String  lan= request.getParameter("lan");
	    String  google_key= request.getParameter("google_key");
	   // String dateandtime=request.getParameter("dataandtime");
	   /// System.out.println("date and time"+dateandtime);
	    //   Timestamp  dataandtime= Timestamp.valueOf(request.getParameter("dataandtime"));
	    
	 try
	 {   
		CellHistory cellhistory=new CellHistory();
		
	    CellData celldata = new CellData();
	    celldata.setCountry_id(country_id);
	    celldata.setMobileno(mobileno);
	    celldata.setOperatername(operatername);
	    celldata.setImsi(imsi);
	    celldata.setLac(lac);
	    celldata.setMcc(mcc);
	    celldata.setMnc(mnc);
	    celldata.setCell_id(cell_id);
	    celldata.setLat(lat);
	    celldata.setLan(lan);
	    celldata.setGoogle_key(google_key);
	   
	    cellhistory.setCountry_id(country_id);
	    cellhistory.setMobileno(mobileno);
	    cellhistory.setOperatername(operatername);
	    cellhistory.setImsi(imsi);
	    cellhistory.setLac(lac);
	    cellhistory.setMcc(mcc);
	    cellhistory.setMnc(mnc);
	    cellhistory.setCell_id(cell_id);
	    cellhistory.setLat(lat);
	    cellhistory.setLan(lan);
	    cellhistory.setGoogle_key(google_key);
	    
	    Date date = new Date();
	    
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	   
	    Timestamp temp=new Timestamp(date.getTime());
	    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//dd/MM/yyyy
	    Date now = new Date();
	    String strDate = sdfDate.format(now);
	    Timestamp ts = Timestamp.valueOf(strDate);
	 //   System.out.println("asdd"+strDate);
	  sdf.format(temp);
	  
	  
	  //  System.out.println("the s"+temp);
	  cellhistory.setDateandtime(ts);
	  cellhistory.setImei(imei);
	  celldata.setDateandtime(ts);
	  celldata.setAcccesstime(ts);
	  celldata.setImei(imei);
	  celldata.setPingstatus("offline");
	  celldataDAO.insertHistory(cellhistory);
	
	   if (!imei.equalsIgnoreCase("NA") || !imei.isEmpty()){
		  
		  
		   int resultsize=celldataDAO.imeisearch(imei);
		 
		 if(resultsize==1){
			
			  celldataDAO.updatecellData(celldata);
			  out.println("SUCCESS Successfully updated");
		  }
		  
	   
	   
	 if(resultsize==0){
		
		int result =  celldataDAO.insertCellData(celldata);
		out.println("SUCCESS  Record is insert in CellDataBase");
	  
	 
	   }
	   
	if(imei=="NA" || imei.isEmpty())
		
	{
		out.println("FAILED IMEI Not Found");
		
	}
		
		
	   }
	   }
	   catch (Exception e) {
			e.printStackTrace();
			out.print("Some Error. Try again!!");
		}	
	  
	  }

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
             PrintWriter out = response.getWriter();
		     response.setContentType("text/html");
		     CellDataDAO celldatadao = new CellDataDAO();
		   String option=  request.getParameter("option");
		   if(option.equalsIgnoreCase("refresh"))
		   {
			   String imei=request.getParameter("imei");
			   String ping=celldatadao.getsuspectkey(imei).getPingstatus();
			   out.println(ping);
		   }
		   if(option.equalsIgnoreCase("getgcm"))
		   {
		
		
		String gcmmasterkey=celldatadao.getmasterkey().getGoogle_key();
	//	System.out.println("hello1");
		
		String imei=request.getParameter("imei");
		celldatadao.setpingstatus(imei);
		String msg=request.getParameter("message");
	
		System.out.println("the imei"+imei);
		String userkey=celldatadao.getsuspectkey(imei).getGoogle_key();
		System.out.println("the gcmkey=="+gcmmasterkey);
		System.out.println("the userkey==="+userkey);
		
		
		
		
		URL myURL3 = new URL("https://android.googleapis.com/gcm/send");
		 HttpURLConnection conn = (HttpURLConnection) myURL3.openConnection();
	        conn.setDoOutput(true);
	        conn.setRequestMethod("POST");
	        conn.setRequestProperty("Content-Type", "application/json");
	     //  String  masterkey="AIzaSyABQ2odu3Kx1Eqh_mUIzD2PC9pljEqVn-g";
	        conn.setRequestProperty("Authorization", "key="+gcmmasterkey+"");
		
	        
	        
	        String body = "{\"notification\": {\"title\": \"CellTracker\",\"text\": \""+msg+"\"},\"to\" : \""+userkey+"\"}";
	      
	        
	        System.out.println("the body =="+body);
	        OutputStream os = conn.getOutputStream();
	        os.write(body.getBytes());
	        os.flush();
	        
	        BufferedReader br = new BufferedReader(new InputStreamReader(
	                (conn.getInputStream())));
	        String googleresposne = br.readLine();
	        
	        JSONObject jsonObj;
			try {
				jsonObj = new JSONObject(googleresposne);
				int success = jsonObj.getInt("success");
				int failure = jsonObj.getInt("failure");
				
				 System.out.println("successcount="+success); 
				 System.out.println("failurecount=="+failure);
				 if(msg.equalsIgnoreCase("getping")){
				 String ping=celldatadao.getsuspectkey(imei).getPingstatus();
				 JSONObject obj = new JSONObject();

			      obj.put("ping", ping);
			      obj.put("success", success);
			      obj.put("failure", failure);
				 out.println(obj);  
				 }
			        System.out.println("Output from Server .... \n"+googleresposne);
			     
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		   }
	       
	        
		}

	@Override
	public void destroy() {
		// resource release
		super.destroy();
	}
	
	}
