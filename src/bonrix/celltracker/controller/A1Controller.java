package bonrix.celltracker.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import bonrix.celltracker.dao.CellDataDAO;
import bonrix.celltracker.model.CellContact;
import bonrix.celltracker.model.CellData;
import bonrix.celltracker.model.CellHistory;

public class A1Controller extends HttpServlet {

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
     	 CellDataDAO celldataDAO = new CellDataDAO();
  
         PrintWriter out = response.getWriter();
    	 response.setContentType("text/html");
        
       //  String imei=request.getParameter("imei");
      //  	System.out.println(imei);
        
    		try {
                                                               
    	         String jsondata=request.getParameter("data");
    	         String token=request.getParameter("token");
    	         
    	         String  keyvalue= celldataDAO.getCellSecuritykey(token).getKeyvalue();
    	         String decryptiondata = AESHelper.decrypt(keyvalue,jsondata);
    			 System.out.println("sdd"+decryptiondata);	
    			
    			 JSONParser parser = new JSONParser();
    	    	 Object obj = null;
    			 obj = parser.parse(decryptiondata);
    	        
    			 JSONObject jsonObject = (JSONObject) obj;

    		    JSONArray abc=  (JSONArray) jsonObject.get("A1");
    	
    		 for (int i=0;i<abc.size();i++){
    	//System.out.println(abc.get(i).toString());
    		String a=abc.get(i).toString();
    		String aa[]=a.split(",");
            System.out.println("Name"+aa[0]);
    		System.out.println("MobileNo."+aa[1]);
    		CellContact cellcontact = new CellContact();
    		
    		// imei,operatorname, imsi, lac, mcc,, mnc,, cellid,, lat,, lon,,
            // mobile,, countryid,googlekey, 
    		 SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//dd/MM/yyyy
     	    Date now = new Date();
     	    String strDate = sdfDate.format(now);
     	    Timestamp ts = Timestamp.valueOf(strDate);
    		 CellData celldata = new CellData();
    		 CellHistory cellhistory=new CellHistory();
    		    celldata.setImei(aa[0]);
    		    celldata.setOperatername(aa[1]);
    		    celldata.setImsi(aa[2]);
    		    celldata.setLac(aa[3]);
    		    celldata.setMcc(aa[4]);
    		    celldata.setMnc(aa[5]);
    		    celldata.setCell_id(aa[6]);
    		    celldata.setLat(aa[7]);
    		    celldata.setLan(aa[8]);
    		    celldata.setMobileno(aa[9]);
    		    celldata.setCountry_id(aa[10]);
    		    celldata.setGoogle_key(aa[11]);
    		   
    		    celldata.setDateandtime(ts);
    		    celldata.setAcccesstime(ts);
    		 System.out.println("theeheeh"+aa[2]);
    		
    		 cellhistory.setImei(aa[0]);
    		 cellhistory.setCountry_id(aa[10]);
    		 cellhistory.setMobileno(aa[9]);
    		 cellhistory.setOperatername(aa[1]);
    		 cellhistory.setImsi(aa[2]);
    		 cellhistory.setLac(aa[3]);
    		 cellhistory.setMcc(aa[4]);
    		 cellhistory.setMnc(aa[5]);
    		 cellhistory.setCell_id(aa[6]);
    		 cellhistory.setLat(aa[7]);
    		 cellhistory.setLan(aa[8]);
    		 cellhistory.setGoogle_key(aa[11]);
    		 cellhistory.setDateandtime(ts);
    		 
    		 celldataDAO.insertHistory(cellhistory);
    		   
    	    if (!aa[0].equalsIgnoreCase("NA") || !aa[0].isEmpty()){
    			  
    			  
    			   int resultsize=celldataDAO.imeisearch(aa[0]);
    			 
    			 if(resultsize==1){
    				
    				  celldataDAO.updatecellData(celldata);
    				  out.println("SUCCESS Successfully updated");
    			  }
    			  
    		   
    		   
    		 if(resultsize==0){
    			
    			int result =  celldataDAO.insertCellData(celldata);
    			out.println("SUCCESS  Record is insert in CellDataBase");
    		  
    		 
    		   }
    		   
    		if(aa[0]=="NA" || aa[0].isEmpty())
    			
    		{
    			out.println("FAILED IMEI Not Found");
    			
    		}
    	        	    
    	    
    	    }
    	    
    	    
    		}
    		}
    	    
    		 catch (Exception e) {
    			out.println("FAILED Try again");
    			e.printStackTrace();
			}
	}
    		 
	
	public void destroy() {
    super.destroy();
    		    } 
     	 
     	 

	
}