package bonrix.celltracker.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import bonrix.celltracker.dao.CellDataDAO;
import bonrix.celltracker.model.CellContact;

public class CellContactController
extends HttpServlet {
    private static final long serialVersionUID = 1;
    CellDataDAO celldataDAO = new CellDataDAO();
 
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	 response.setContentType("text/html;charset=UTF-8");
         PrintWriter out = response.getWriter();
    	String option =request.getParameter("option");
    	if(option.equalsIgnoreCase("deletedata"))
    	{
       String imei=request.getParameter("imei");
      String result=celldataDAO.deletedata(imei);
        
      
        out.println(result);
        
    	}
    	
    	
    	if(option.equalsIgnoreCase("getContact"))
    	{
    	int page = 1;
		int listSize = 10;
		int start = 0;
		int len = 0;
		String searchdata=request.getParameter("searchdata");
		System.out.println("the dadddddddddd"+searchdata);
		
		if (request.getParameter("start") != null) {
			start = Integer.parseInt(request.getParameter("start"));
			System.out.println("STSRT : "+start);
		}
				
		else
		{System.out.println("STSRT : "+start);}

		if (request.getParameter("length") != null) {
			len = Integer.parseInt(request.getParameter("length"));
			System.out.println("LENGTH : "+len);
			listSize = len;
		}
		else
		{System.out.println("LENGTH : "+len);}
		page = start / len + 1;
		
		if(searchdata.equalsIgnoreCase("NA")){
    		System.out.println("hello");
    		String result = celldataDAO.getcontactlist(page,listSize,start);
    		System.out.println("the"+result);
			out.println(result);
		}	
		if(!searchdata.equalsIgnoreCase("NA"))	{
			
			String result2 = celldataDAO.getsearchlist(page,listSize,start,searchdata);
    		System.out.println("the"+result2);
			out.println(result2);
			}
    	}
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	//////////////////tokan code/////////////
    	/*URL aURL = new URL("http://example.com:80/docs/books/tutorial"
                + "/index.html?name=networking#DOWNLOADING");

System.out.println("protocol = " + aURL.getProtocol()); //http
System.out.println("authority = " + aURL.getAuthority()); //example.com:80
System.out.println("host = " + aURL.getHost()); //example.com
System.out.println("port = " + aURL.getPort()); //80
System.out.println("path = " + aURL.getPath()); //  /docs/books/tutorial/index.html
System.out.println("query = " + aURL.getQuery()); //name=networking
System.out.println("filename = " + aURL.getFile()); ///docs/books/tutorial/index.html?name=networking
System.out.println("ref = " + aURL.getRef()); //DOWNLOADING 
 URL url = new URL(""+request.getRequestURL()+"");*/

	 PrintWriter out = response.getWriter();
	 response.setContentType("text/html");
    
     String jsondata=request.getParameter("data");
     String imei=request.getParameter("imei");
    	System.out.println(imei);
    
		try {
			JSONParser parser = new JSONParser();
	    	Object obj = null;
			obj = parser.parse(jsondata);
		

		JSONObject jsonObject = (JSONObject) obj;

		JSONArray abc=  (JSONArray) jsonObject.get("contacts");
	
		for (int i=0;i<abc.size();i++){
		
			//System.out.println(abc.get(i).toString());
			String a=abc.get(i).toString();
		String aa[]=a.split(",");
		
		System.out.println("Name"+aa[0]);
		System.out.println("MobileNo."+aa[1]);
		CellContact cellcontact = new CellContact();
		cellcontact.setImei(imei);
		cellcontact.setName(aa[0]);
		cellcontact.setMobileno(aa[1]);
		
		System.out.println("theeheeh"+aa[2]);
		cellcontact.setWhatsapp(aa[2]);
	    
	    
	  
	    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//dd/MM/yyyy
	    Date now = new Date();
	    String strDate = sdfDate.format(now);
	    Timestamp ts = Timestamp.valueOf(strDate);
	    
	    cellcontact.setDateandtime(ts);
	  
	    
	 //   int resultsize =  celldataDAO.imeimobilesearch(cellcontact);  
	   
	int result =  celldataDAO.insertCellContact(cellcontact);

		System.out.println(""+result);
		out.println("SUCCESS Successfully inserted");
		}
	   
		
		
		} catch (ParseException e) {
			out.println("FAILED Try again");
			e.printStackTrace();
		}
		
			
		
    
    }

    public void destroy() {
        super.destroy();
    }
}