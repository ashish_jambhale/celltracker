package bonrix.celltracker.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bonrix.celltracker.dao.LoginDAO;
import bonrix.celltracker.model.Login;

public class LoginController  extends HttpServlet  {

	
	private static final long serialVersionUID = 1L;

	@Override
	public void init() throws ServletException {
		// Servlet initialization code here
		super.init();
	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

	response.setContentType("text/html");  
    PrintWriter out=response.getWriter();  
    HttpSession session=request.getSession();  
    String username=request.getParameter("username");  
    String password=request.getParameter("password");  
    String option=request.getParameter("option");
    
    System.out.println("Name:"+username);
    System.out.println("Password:"+password);
    
       LoginDAO loginDAO = new LoginDAO();
	  String result =  loginDAO.loginCheck(username,password);
   
	  int y = Integer.parseInt(result);
	 	
   System.out.println("result45555:= "+y);
  //out.println(" Record is insert in errorreport"+result);
    
    if(y==1){  
    out.print("Welcome, "+username);  
   
    session.setAttribute("name",username); 
    session.setAttribute("password",password); 
    response.sendRedirect(request.getContextPath() + "/dashboard.jsp");
  
    }  
    else{
    	
        out.print("<h1>Sorry, Username or Password is Incorrect!</h1>");  
       request.getRequestDispatcher("login.jsp").include(request, response);  
    
      
       }  
    out.close();  

	}
@Override
	public void destroy() {
		// resource release
		super.destroy();
	}
	
	
	}

	