package bonrix.celltracker.model;

import java.sql.Timestamp;
import java.util.Date;

public class CellData {
	private Integer celltracker_id;
	
     private String imei;
     private String country_id;
     private String mobileno;
	
	private String operatername;
	 private String imsi;
	 private String lac;
	 private String mcc;
	 private String mnc;
	 private String cell_id;
	 private String lat;
	 private String lan;
	 private Timestamp dateandtime;
	 private Timestamp acccesstime;
	 private String google_key;
	 private String pingstatus;

	 
	  public String getPingstatus() {
		return pingstatus;
	}
	public void setPingstatus(String pingstatus) {
		this.pingstatus = pingstatus;
	}
	public String getGoogle_key() {
		return google_key;
	}
	public void setGoogle_key(String google_key) {
		this.google_key = google_key;
	}
	public Timestamp getAcccesstime() {
		return acccesstime;
	}
	public void setAcccesstime(Timestamp acccesstime) {
		this.acccesstime = acccesstime;
	}
	public Integer getCelltracker_id() {
			return celltracker_id;
		}
		public void setCelltracker_id(Integer celltracker_id) {
			this.celltracker_id = celltracker_id;
		}
	public Timestamp getDateandtime() {
		return dateandtime;
	}
	public void setDateandtime(Timestamp dateandtime) {
		this.dateandtime = dateandtime;
	}


	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	 public String getCountry_id() {
			return country_id;
		}
		public void setCountry_id(String country_id) {
			this.country_id = country_id;
		}
		public String getMobileno() {
			return mobileno;
		}
		public void setMobileno(String mobileno) {
			this.mobileno = mobileno;
		}
	public String getOperatername() {
		return operatername;
	}
	public void setOperatername(String operatername) {
		this.operatername = operatername;
	}
	public String getImsi() {
		return imsi;
	}
	public void setImsi(String imsi) {
		this.imsi = imsi;
	}
	public String getLac() {
		return lac;
	}
	public void setLac(String lac) {
		this.lac = lac;
	}
	public String getMcc() {
		return mcc;
	}
	public void setMcc(String mcc) {
		this.mcc = mcc;
	}
	public String getMnc() {
		return mnc;
	}
	public void setMnc(String mnc) {
		this.mnc = mnc;
	}
	public String getCell_id() {
		return cell_id;
	}
	public void setCell_id(String cell_id) {
		this.cell_id = cell_id;
	}
	public String getLat() {
		return lat;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}
	public String getLan() {
		return lan;
	}
	public void setLan(String lan) {
		this.lan = lan;
	}

	 
}
