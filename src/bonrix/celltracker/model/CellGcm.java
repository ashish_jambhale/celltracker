package bonrix.celltracker.model;

import java.sql.Timestamp;

public class CellGcm {
	private int gcm_id;
	public int getGcm_id() {
		return gcm_id;
	}

	public void setGcm_id(int gcm_id) {
		this.gcm_id = gcm_id;
	}

	private String username;
	 private String password;
	 private String google_key;
	 public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getGoogle_key() {
		return google_key;
	}

	public void setGoogle_key(String google_key) {
		this.google_key = google_key;
	}

	
	
	
	
	
}
