package bonrix.celltracker.model;

import java.sql.Timestamp;

public class CellContact {

	private Integer contact_id;
	
    private String imei;
    private String mobileno;
    private String name;
    private String whatsapp;
    public String getWhatsapp() {
		return whatsapp;
	}
	public void setWhatsapp(String whatsapp) {
		this.whatsapp = whatsapp;
	}
	private Timestamp dateandtime;
    
	public Timestamp getDateandtime() {
		return dateandtime;
	}
	public void setDateandtime(Timestamp dateandtime) {
		this.dateandtime = dateandtime;
	}
	public Integer getContact_id() {
		return contact_id;
	}
	public void setContact_id(Integer contact_id) {
		this.contact_id = contact_id;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public String getMobileno() {
		return mobileno;
	}
	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}
