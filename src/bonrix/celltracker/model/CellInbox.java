package bonrix.celltracker.model;

import java.sql.Timestamp;

public class CellInbox {

private Integer inbox_id;
private String sms_id;
    private String imei;
   
    private String address;
    private String smsbody;
    private String dateandtime;
	public Integer getInbox_id() {
		return inbox_id;
	}
	public void setInbox_id(Integer inbox_id) {
		this.inbox_id = inbox_id;
	}
	public String getSms_id() {
		return sms_id;
	}
	public void setSms_id(String sms_id) {
		this.sms_id = sms_id;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getSmsbody() {
		return smsbody;
	}
	public void setSmsbody(String smsbody) {
		this.smsbody = smsbody;
	}
	public String getDateandtime() {
		return dateandtime;
	}
	public void setDateandtime(String dateandtime) {
		this.dateandtime = dateandtime;
	}
	
	
}
