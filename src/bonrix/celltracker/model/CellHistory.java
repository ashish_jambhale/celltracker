package bonrix.celltracker.model;

import java.sql.Timestamp;

public class CellHistory {
	private Integer cellhistory_id;
	
    private String imei;
    private String country_id;
    private String mobileno;
	
	private String operatername;
	 private String imsi;
	 private String lac;
	 private String mcc;
	 private String mnc;
	 private String cell_id;
	 private String lat;
	 private String lan;
	 private Timestamp dateandtime;
	 private String google_key;
	 
	public String getGoogle_key() {
		return google_key;
	}
	public void setGoogle_key(String google_key) {
		this.google_key = google_key;
	}
	public Integer getCellhistory_id() {
		return cellhistory_id;
	}
	public void setCellhistory_id(Integer cellhistory_id) {
		this.cellhistory_id = cellhistory_id;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public String getCountry_id() {
		return country_id;
	}
	public void setCountry_id(String country_id) {
		this.country_id = country_id;
	}
	public String getMobileno() {
		return mobileno;
	}
	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}
	public String getOperatername() {
		return operatername;
	}
	public void setOperatername(String operatername) {
		this.operatername = operatername;
	}
	public String getImsi() {
		return imsi;
	}
	public void setImsi(String imsi) {
		this.imsi = imsi;
	}
	public String getLac() {
		return lac;
	}
	public void setLac(String lac) {
		this.lac = lac;
	}
	public String getMcc() {
		return mcc;
	}
	public void setMcc(String mcc) {
		this.mcc = mcc;
	}
	public String getMnc() {
		return mnc;
	}
	public void setMnc(String mnc) {
		this.mnc = mnc;
	}
	public String getCell_id() {
		return cell_id;
	}
	public void setCell_id(String cell_id) {
		this.cell_id = cell_id;
	}
	public String getLat() {
		return lat;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}
	public String getLan() {
		return lan;
	}
	public void setLan(String lan) {
		this.lan = lan;
	}
	public Timestamp getDateandtime() {
		return dateandtime;
	}
	public void setDateandtime(Timestamp dateandtime) {
		this.dateandtime = dateandtime;
	}
}
