package util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {

	public static String format(Date date, String format) {		
		DateFormat formater = new SimpleDateFormat(format);
		return formater.format(date);
	}
}
